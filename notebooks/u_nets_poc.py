# Dataset - LIDC-IDRI consisting of diagnostic and lung cancer screening thoracic CT scans with marked-up annotated lesions.
# This script is intended to serve as PoC for an SCP Slurm Demo
# This script will ask the user for a patient ID, create a dataframe consisting of all image slices belonging to that patient, the image's pixel array, and the corresponding segemntation mask to go with that slice if an annotation is present. That will then be fed into a U-net model. 

import pandas as pd
import configparser
import sys

import pydicom
import pylidc as pl
from pylidc.utils import consensus
from skimage.measure import find_contours
import matplotlib.pyplot as plt
import numpy as np
import os
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import pandas as pd
import tensorflow as tf
import statistics
from sklearn.model_selection import train_test_split
import warnings
warnings.filterwarnings("ignore")

from keras.models import *
from keras.layers import *
from keras.optimizers import *
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as keras

print("All libraries have been imported")
#parsing the config file
def parse_config(file):
        #global config_dictionary
        config_dictionary=dict()
        config=configparser.ConfigParser()
        config.optionxform = str

        config.read(file)
        for section in config.sections():
                config_dictionary[section] = {}
                for option in config.options(section):
                        config_dictionary[section][option] = config.get(section, option)
        return config_dictionary    


#U net function
def unet(pretrained_weights = None,input_size = (512,512,1)):
    inputs = Input(input_size)
    conv1 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
    conv1 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
    conv2 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
    conv2 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
    conv3 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
    conv4 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
    conv4 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
    drop4 = Dropout(0.5)(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

    conv5 = Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
    conv5 = Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
    drop5 = Dropout(0.5)(conv5)

    up6 = Conv2D(512, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(drop5))
    merge6 = concatenate([drop4,up6], axis = 3)
    conv6 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
    conv6 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)

    up7 = Conv2D(256, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv6))
    merge7 = concatenate([conv3,up7], axis = 3)
    conv7 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
    conv7 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)

    up8 = Conv2D(128, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv7))
    merge8 = concatenate([conv2,up8], axis = 3)
    conv8 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
    conv8 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

    up9 = Conv2D(64, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv8))
    merge9 = concatenate([conv1,up9], axis = 3)
    conv9 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
    conv9 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv9 = Conv2D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv10 = Conv2D(1, 1, activation = 'sigmoid')(conv9)

    model = Model(inputs = inputs, outputs = conv10)
    IOUmetric = tf.keras.metrics.MeanIoU(num_classes=2)
    model.compile(optimizer = tf.keras.optimizers.Adam(learning_rate = 1e-4), loss = 'binary_crossentropy', metrics = ['accuracy', IOUmetric ])
    
    #model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)

    return model

def load_scans(_dcm_path):
    """helper function to load image slices for a patient"""

    _files = os.listdir( _dcm_path )
    _file_nums = [_file.split(".")[0] for _file in _files if '.dcm' in _file]
    _file_nums_sorted = sorted(_file_nums, reverse=True)
        
    _slices = [pydicom.dcmread(_dcm_path + "/" + str(_file_num) + ".dcm" ) for _file_num in _file_nums_sorted]

    return _slices

def rescale(pixArray):
    max_val = np.max(pixArray)
    pixArray = pixArray/max_val
    return pixArray


config_mat = parse_config(sys.argv[1])
print("Information from our config file: ")
print("Patient id: ", config_mat['Patient']['id'] )
print("Batch size: ", config_mat['Model']['batch_size'])
print("# of epochs: ", config_mat['Model']['N_epochs'])

scan = pl.query(pl.Scan).filter(pl.Scan.patient_id == config_mat['Patient']['id']).first()
totalSlices = int(load_scans(scan.get_path_to_dicom_files())[0].InstanceNumber)
#all anots for one scan for patient 
vol = scan.to_volume()
images = scan.load_all_dicom_images()

# Cluster the annotations for the scan
nods = scan.cluster_annotations()

#An example using consensus mask
anns = nods[0] #grab one cluster of annotations
x_centroid = []
y_centroid = []
for annot in anns:
    x_centroid.append(int(annot.centroid[0]))
    y_centroid.append(int(annot.centroid[1]))
avg_x = int(statistics.mean(x_centroid))
avg_y = int(statistics.mean(y_centroid))
padding = [(avg_x,512-avg_x), (avg_y,512-avg_y), (0,0)]
cmask,cbbox,masks = consensus(anns, clevel=0.5, pad = padding)
#cmask,cbbox,masks = consensus(anns, clevel=0.5)
# Get the central slice of the computed bounding box.
k = int(0.5*(cbbox[2].stop - cbbox[2].start))
# Set up the plot.
print("This is an example of a mask around an annotation")
fig,ax = plt.subplots(1,3,figsize=(5,5))
ax[0].imshow(vol[cbbox][:,:,k], cmap=plt.cm.gray, alpha=0.5)
ax[0].axis('off')
ax[1].imshow(cmask[:,:,k], cmap = plt.cm.gray)
ax[1].axis('off')
ax[2].imshow(cmask[:,:,k], cmap = plt.cm.gray)
ax[2].imshow(vol[cbbox][:,:,k], cmap=plt.cm.gray, alpha=0.5)
ax[2].axis('off')
plt.show(block=True)
print(anns[0].centroid)

## get all the masks with the corresponding slices
slices = []
slicesMaskDic = {}
for x in range(0, len(nods)):
    anns = nods[x]
    #take average of all the centroid values to get padding
    x_centroid = []
    y_centroid = []
    for annot in anns:
        x_centroid.append(int(annot.centroid[0]))
        y_centroid.append(int(annot.centroid[1]))
    avg_x = int(statistics.mean(x_centroid))
    avg_y = int(statistics.mean(y_centroid))
    padding = [(avg_x,512-avg_x), (avg_y,512-avg_y), (0,0)]
    cmask,cbbox,masks = consensus(anns, clevel=0.5, pad = padding)
    # Get the central slice of the computed bounding box.
    k = int(0.5*(cbbox[2].stop - cbbox[2].start))
    all_annots = []
    for annot in anns:
        all_annots.append(list(annot.contour_slice_indices))
    #find the most common slice ids across where this mask holds and applies
    result = set(all_annots[0])
    for s in all_annots[1:]:
        result.intersection_update(s)
    slice_ids = (list(result))
    slices.append(slice_ids)
    #Dictionary to store output of the cbbox, and cmasks
    #if a slice is repeated across multiple nodules, store the latest mask value
    for sc in slice_ids:
        slicesMaskDic[sc] = [vol[cbbox][:,:,k], cmask[:,:,k ]]
flat_list = [item for sublist in slices for item in sublist]
print("Slices which contain an annotation:",flat_list)

##create dataframe
sliceID = list(range(1, totalSlices+1))
patientSlicesDF = pd.DataFrame(sliceID, columns = ['Slice ID'])
patientSlicesDF['Annot present'] = patientSlicesDF['Slice ID'].apply(lambda x: 1 if x in flat_list else 0)
#convert 512x512 image into a matrix of size 512 x 512 x 1 which is fed into the network.
patientSlicesDF['Pixel array'] = patientSlicesDF['Slice ID'].apply(lambda x: load_scans(scan.get_path_to_dicom_files())[x-1].pixel_array)
patientSlicesDF['Pixel array'] = patientSlicesDF['Pixel array'].apply(lambda x: x.reshape(512,512,1))
#rescale
patientSlicesDF['Pixel array'] = patientSlicesDF['Pixel array'].apply(lambda x: rescale(x))
#this is our target 
patientSlicesDF['Mask'] = patientSlicesDF['Slice ID'].apply(lambda x: slicesMaskDic[x][1] if x in slicesMaskDic.keys() else np.zeros((512,512)) )
patientSlicesDF['Bbox'] = patientSlicesDF['Slice ID'].apply(lambda x: slicesMaskDic[x][0] if x in slicesMaskDic.keys() else  np.zeros((512,512)) )
#convert arrays to 0 and 1s
patientSlicesDF['Mask'] = patientSlicesDF['Mask'].apply(lambda x: (x==True).astype(int) )
#reshape our target into a matrix of size 512 x 512 x 1
patientSlicesDF['Mask'] = patientSlicesDF['Mask'].apply(lambda x: x.reshape(512,512,1) )

#training and test split
breakPoint = int((int(config_mat['Model']['training_split'])/100)*totalSlices)
train_X = patientSlicesDF['Pixel array'][0:breakPoint]
train_Y = patientSlicesDF['Mask'][0:breakPoint]
test_X = patientSlicesDF['Pixel array'][breakPoint:]
test_Y = patientSlicesDF['Mask'][breakPoint:]

train_X = np.array(train_X.values.tolist())
train_Y = np.array(train_Y.values.tolist())
test_X = np.array(test_X.values.tolist())
test_Y = np.array(test_Y.values.tolist())

#split the training data into two parts, 80% training and 20% for validation
from sklearn.model_selection import train_test_split
train_X,valid_X,train_label,valid_label = train_test_split(train_X, train_Y, test_size=0.2, random_state=13)

print(patientSlicesDF.head(5))

model = unet()
batch_size = int(config_mat['Model']['batch_size'])
epochs = int(config_mat['Model']['N_epochs'])
train = model.fit(train_X, train_label, batch_size=batch_size,epochs=epochs,verbose=1,validation_data=(valid_X,valid_label))

test_eval = model.evaluate(test_X, test_Y, verbose=0)
print('Test loss:', test_eval[0])
print('Test accuracy:', test_eval[1])

accuracy = train.history['accuracy']
val_accuracy = train.history['val_accuracy']
loss = train.history['loss']
val_loss = train.history['val_loss']
epochs = range(len(accuracy))
plt.plot(epochs, accuracy, 'b--', label='Training accuracy')
plt.plot(epochs, val_accuracy, 'b', label='Validation accuracy')
plt.yscale('log')
plt.ylabel('Log scale')
plt.xlabel('# of epochs')
plt.title('Training and validation accuracy')
plt.legend()
plt.figure()
plt.plot(epochs, loss, 'b--', label='Training loss')
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('Training and validation loss')
plt.yscale('log')
plt.ylabel('Log scale')
plt.xlabel('# of epochs')
plt.legend()
plt.show()

