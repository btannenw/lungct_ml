import configparser
import sys

import pydicom
import pylidc as pl
import matplotlib.pyplot as plt
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import pandas as pd
import tensorflow as tf
import statistics
from sklearn.model_selection import train_test_split
import warnings
warnings.filterwarnings("ignore")
import statistics
from pylidc.utils import consensus
from skimage.measure import find_contours

sys.path.insert(0, '/home/kckr575/lungct_ml')
from data.load_dataset import nodsPat, returnMasks, createDF, createDF_half, rescale, returnMasks_T

from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import concatenate
from tensorflow.keras.layers import Dense,UpSampling2D, Activation, Dropout, BatchNormalization, Flatten, Conv2D, MaxPooling2D, AveragePooling2D, Input

print("All libraries have been imported")

def unet(pretrained_weights = None,input_size = (512,512,1)):
    inputs = Input(input_size)
    conv1 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
    conv1 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
    conv2 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
    conv2 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
    conv3 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
    conv4 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
    conv4 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
    drop4 = Dropout(0.5)(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

    conv5 = Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
    conv5 = Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
    drop5 = Dropout(0.5)(conv5)

    up6 = Conv2D(512, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(drop5))
    merge6 = concatenate([drop4,up6], axis = 3)
    conv6 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
    conv6 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)

    up7 = Conv2D(256, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv6))
    merge7 = concatenate([conv3,up7], axis = 3)
    conv7 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
    conv7 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)

    up8 = Conv2D(128, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv7))
    merge8 = concatenate([conv2,up8], axis = 3)
    conv8 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
    conv8 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

    up9 = Conv2D(64, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv8))
    merge9 = concatenate([conv1,up9], axis = 3)
    conv9 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
    conv9 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv9 = Conv2D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv10 = Conv2D(1, 1, activation = 'sigmoid')(conv9)

    model = Model(inputs = inputs, outputs = conv10)
    #IOUmetric = tf.keras.metrics.MeanIoU(num_classes=2)
    model.compile(optimizer = tf.keras.optimizers.Adam(), loss = 'binary_crossentropy', metrics = ['accuracy'])
    
    #model.summary()

    if(pretrained_weights):
    	model.load_weights(pretrained_weights)

    return model

#parsing the config file
def parse_config(file):
        #global config_dictionary
        config_dictionary=dict()
        config=configparser.ConfigParser()
        config.optionxform = str

        config.read(file)
        for section in config.sections():
                config_dictionary[section] = {}
                for option in config.options(section):
                        config_dictionary[section][option] = config.get(section, option)
        return config_dictionary    
    
input_imgs = np.zeros([1,512,512,1])
label_imgs = np.zeros([1,512,512,1])
patID = [str(item).zfill(4) for item in list(range(1, int(config_dictionary['Patient']['numPat'])))]
for pat in patID:
    nods_pat, vol_pat, images_pat, totalSlices_pat, scan_pat = nodsPat('LIDC-IDRI-' + pat) 
    slicesMaskDic_pat = returnMasks_T(nods_pat, vol_pat)
    # create np arrays of slices for a single scan
    input_imgs_pat_first = np.moveaxis(vol_pat, -1, 0) # shuffle so last dim (slice) is first
    input_imgs_pat_first = np.expand_dims(input_imgs_pat_first, axis=-1)
    nSlices = input_imgs_pat_first.shape[0]
    # get slices indices which contain annotations
    sliceIDs = []
    for _ in range(0, len(scan_pat.annotations)):
        for n in range(0, len(scan_pat.annotations[_].contour_slice_indices)): 
            #print(_, n, scan.annotations[_].contour_slice_indices[n])
            sliceIDs.append(scan_pat.annotations[_].contour_slice_indices[n])
    sliceIndicesWithAnnotations = set(sliceIDs)
    
    # create labels
    input_masks_pat_first = np.zeros([nSlices, 512,512,1])
    for _ in range(0, nSlices):
        if _ in sliceIndicesWithAnnotations:
            input_masks_pat_first[_] = slicesMaskDic_pat[_][1]
    input_masks_pat_first.shape
    ## rescale pixel arrays
    for ar in range(0, len(input_imgs_pat_first)):
        input_imgs_pat_first[ar] = rescale(input_imgs_pat_first[ar])
    
    #concatenate everything together
    input_imgs = np.concatenate((input_imgs,input_imgs_pat_first))
    label_imgs = np.concatenate((label_imgs,input_masks_pat_first))
    
    print('Length of patient' + ' ' + pat + ":", len(input_imgs_pat_first))

## training and testing
train_X,test_X,train_label,test_label = train_test_split(input_imgs, label_imgs, test_size = int(config_dictionary['Model']['training_split']))
##validation
train_X,valid_X,train_label,valid_label = train_test_split(train_X, train_label, test_size=int(config_dictionary['Model']['valid_split']), random_state=13)
print(train_X.shape, test_X.shape, valid_X.shape, train_label.shape, test_label.shape, valid_label.shape)

model = unet()
batch_size = int(config_dictionary['Model']['batch_size'])
epochs = int(config_dictionary['Model']['N_epochs'])
train = model.fit(train_X, train_label, batch_size=batch_size,epochs=epochs,verbose=1,validation_data=(valid_X, valid_label))
test_eval = model.evaluate(test_X, test_label, verbose=0)
print('Test loss:', test_eval[0])
print('Test accuracy:', test_eval[1])