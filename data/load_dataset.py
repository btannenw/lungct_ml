import pydicom
import pylidc as pl
from pylidc.utils import consensus
from skimage.measure import find_contours
import numpy as np
import pandas as pd
import statistics
import os

def load_scans(_dcm_path):
    """helper function to load image slices for a patient"""

    _files = os.listdir( _dcm_path )
    _file_nums = [_file.split(".")[0] for _file in _files if '.dcm' in _file]
    _file_nums_sorted = sorted(_file_nums, reverse=True)
        
    _slices = [pydicom.dcmread(_dcm_path + "/" + str(_file_num) + ".dcm" ) for _file_num in _file_nums_sorted]

    return _slices

def rescale(pixArray):
    max_val = np.max(pixArray)
    pixArray = pixArray/max_val
    return pixArray

## one patient - return nodules, vol, and images relating to one patient
def nodsPat(patID):
    scan = pl.query(pl.Scan).filter(pl.Scan.patient_id == patID).first()
    totalSlices = int(load_scans(scan.get_path_to_dicom_files())[0].InstanceNumber)
    #all anots for one scan for patient 
    vol = scan.to_volume()
    images = scan.load_all_dicom_images()

    # Cluster the annotations for the scan
    nods = scan.cluster_annotations()
    
    return nods, vol, images, totalSlices, scan 


def returnMasks_T(nods,vol):
    slices = []
    slicesMaskDic = {}
    for x in range(0, len(nods)):
        anns = nods[x]
        #take average of all the centroid values to get padding
        x_centroid = []
        y_centroid = []
        for annot in anns:
            x_centroid.append(int(annot.centroid[0]))
            y_centroid.append(int(annot.centroid[1]))
        avg_x = int(statistics.mean(x_centroid))
        avg_y = int(statistics.mean(y_centroid))
        padding = [(avg_x,512-avg_x), (avg_y,512-avg_y), (0,0)]
        cmask,cbbox,masks = consensus(anns, clevel=0.5, pad = padding)
        # Get the central slice of the computed bounding box.
        k = int(0.5*(cbbox[2].stop - cbbox[2].start))
        #print(k, annot.contour_slice_indices)
        #print(cmask[:,:,k ].shape)
        #print(padding)
        all_annots = []
        for annot in anns:
            all_annots.append(list(annot.contour_slice_indices))
            #print(all_annots)
            #find the most common slice ids across where this mask holds and applies
        flatLT = [item for sublist in all_annots for item in sublist]
        result = set(flatLT)
        slice_ids = (list(result))
        slices.append(slice_ids)
            #Dictionary to store output of the cbbox, and cmasks
            #if a slice is repeated across multiple nodules, store the latest mask value
        for sc in slice_ids:
            slicesMaskDic[sc] = [vol[cbbox][:,:,k], cmask[:,:,k ]]
        for val in slicesMaskDic:
        #print(slicesMaskDic[val][1].shape)
            slicesMaskDic[val][1] = (slicesMaskDic[val][1]==True).astype(int)
            slicesMaskDic[val][1] = slicesMaskDic[val][1].reshape(512,512,1)
    return slicesMaskDic 

def returnMasks(nods, vol):
    slices = []
    slicesMaskDic = {}
    for x in range(0, len(nods)):
        anns = nods[x]
        #take average of all the centroid values to get padding
        x_centroid = []
        y_centroid = []
        for annot in anns:
            x_centroid.append(int(annot.centroid[0]))
            y_centroid.append(int(annot.centroid[1]))
        avg_x = int(statistics.mean(x_centroid))
        avg_y = int(statistics.mean(y_centroid))
        padding = [(avg_x,512-avg_x), (avg_y,512-avg_y), (0,0)]
        cmask,cbbox,masks = consensus(anns, clevel=0.5, pad = padding)
        # Get the central slice of the computed bounding box.
        k = int(0.5*(cbbox[2].stop - cbbox[2].start))
        all_annots = []
        for annot in anns:
            all_annots.append(list(annot.contour_slice_indices))
        #find the most common slice ids across where this mask holds and applies
        result = set(all_annots[0])
        for s in all_annots[1:]:
            result.intersection_update(s)
        slice_ids = (list(result))
        slices.append(slice_ids)
        #Dictionary to store output of the cbbox, and cmasks
        #if a slice is repeated across multiple nodules, store the latest mask value
        for sc in slice_ids:
            slicesMaskDic[sc] = [vol[cbbox][:,:,k], cmask[:,:,k ]]

        flat_list = [item for sublist in slices for item in sublist]
    
    return slicesMaskDic, flat_list

def createDF(scan, totalSlices,slicesMaskDic, flat_list):
    sliceID = list(range(1, totalSlices+1))
    patientSlicesDF = pd.DataFrame(sliceID, columns = ['Slice ID'])
    patientSlicesDF['Annot present'] = patientSlicesDF['Slice ID'].apply(lambda x: 1 if x in flat_list else 0)
    #convert 512x512 image into a matrix of size 512 x 512 x 1 which is fed into the network.
    patientSlicesDF['Pixel array'] = patientSlicesDF['Slice ID'].apply(lambda x: load_scans(scan.get_path_to_dicom_files())[x-1].pixel_array)
    patientSlicesDF['Pixel array'] = patientSlicesDF['Pixel array'].apply(lambda x: x.reshape(512,512,1))
    #rescale
    patientSlicesDF['Pixel array'] = patientSlicesDF['Pixel array'].apply(lambda x: rescale(x))
    #this is our target 
    patientSlicesDF['Mask'] = patientSlicesDF['Slice ID'].apply(lambda x: slicesMaskDic[x][1] if x in slicesMaskDic.keys() else np.zeros((512,512)) )
    patientSlicesDF['Bbox'] = patientSlicesDF['Slice ID'].apply(lambda x: slicesMaskDic[x][0] if x in slicesMaskDic.keys() else  np.zeros((512,512)) )
    #convert arrays to 0 and 1s
    patientSlicesDF['Mask'] = patientSlicesDF['Mask'].apply(lambda x: (x==True).astype(int) )
    #reshape our target into a matrix of size 512 x 512 x 1
    patientSlicesDF['Mask'] = patientSlicesDF['Mask'].apply(lambda x: x.reshape(512,512,1) )
    
    return patientSlicesDF

def createDF_half(scan, totalSlices,slicesMaskDic, flat_list):
    sliceID = list(range(1, totalSlices+1))
    patientSlicesDF = pd.DataFrame(sliceID, columns = ['Slice ID'])
    patientSlicesDF['Annot present'] = patientSlicesDF['Slice ID'].apply(lambda x: 1 if x in flat_list else 0)
    ### get only those slices where annot is present 
    patientSlicesDF_p = patientSlicesDF.loc[patientSlicesDF['Annot present'] == 1]
    patientSlicesDF_n = patientSlicesDF.loc[patientSlicesDF['Annot present'] == 0]
    ###get corresponding negative slices too
    lenPos = len(patientSlicesDF_p)
    patientSlicesDF_n = patientSlicesDF_n.iloc[:lenPos]
    ##merge those two frames together
    newDF = patientSlicesDF_p.append(patientSlicesDF_n, ignore_index = True)
    newDF.reset_index(inplace = True)
    #convert 512x512 image into a matrix of size 512 x 512 x 1 which is fed into the network.
    newDF['Pixel array'] = newDF['Slice ID'].apply(lambda x: load_scans(scan.get_path_to_dicom_files())[x-1].pixel_array)
    newDF['Pixel array'] = newDF['Pixel array'].apply(lambda x: x.reshape(512,512,1))
    #rescale
    newDF['Pixel array'] = newDF['Pixel array'].apply(lambda x: rescale(x))
    #this is our target 
    newDF['Mask'] = newDF['Slice ID'].apply(lambda x: slicesMaskDic[x][1] if x in slicesMaskDic.keys() else np.zeros((512,512)) )
    newDF['Bbox'] = newDF['Slice ID'].apply(lambda x: slicesMaskDic[x][0] if x in slicesMaskDic.keys() else  np.zeros((512,512)) )
    #convert arrays to 0 and 1s
    newDF['Mask'] = newDF['Mask'].apply(lambda x: (x==True).astype(int) )
    #reshape our target into a matrix of size 512 x 512 x 1
    newDF['Mask'] = newDF['Mask'].apply(lambda x: x.reshape(512,512,1) )
    
    return newDF